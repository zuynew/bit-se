-- Migration: initial_schema
-- ====  UP  ====

BEGIN;

CREATE TABLE tbl_user
(
    id BIGSERIAL PRIMARY KEY,
    email VARCHAR(200),
    password_hash VARCHAR(150) NOT NULL
);
CREATE UNIQUE INDEX tbl_user_email_uindex ON tbl_user (email);

COMMIT;

-- ==== DOWN ====

BEGIN;

COMMIT;

lazy val commonSettings = Seq(
  scalaVersion := "2.11.11"
)

lazy val core = (project in file("core"))
  .settings(commonSettings: _*)
  .settings(
    libraryDependencies :=
      Seq(
        "org.apache.spark" %% "spark-sql" % "2.2.0",
        "org.apache.spark" % "spark-mllib_2.11" % "2.2.0"
      ))

val http4sVersion = "0.17.5"
lazy val server = (project in file("server"))
  .settings(commonSettings: _*)
  .settings(
    libraryDependencies :=
      Seq(
        "org.http4s" %% "http4s-dsl" % http4sVersion,
        "org.http4s" %% "http4s-blaze-server" % http4sVersion,
        "org.http4s" %% "http4s-circe" % http4sVersion,
        "io.circe" % "circe-generic_2.11" % "0.8.0"
      ))
  .dependsOn(core)

//name := "bit-se"
//
//version := "0.1"

//scalaVersion := "2.11.11"

//libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.2.0"
//
//libraryDependencies += "org.apache.spark" % "spark-mllib_2.11" % "2.2.0"
//
//
//libraryDependencies += "com.typesafe.akka" % "akka-http_2.11" % "10.0.10"
//
//libraryDependencies += "io.circe" % "circe-generic_2.11" % "0.8.0"
//
//libraryDependencies += "com.github.finagle" %% "finch-core" % "0.15.1"
//
//libraryDependencies += "com.github.finagle" %% "finch-circe" % "0.15.1"
//

import core.PredictModelService
import io.circe._
import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s._
import org.http4s.dsl._
import org.http4s.circe._
import org.http4s.server.blaze.BlazeBuilder
import fs2.{Stream, Task}
import org.apache.log4j.{Level, Logger}
import org.http4s.util.StreamApp

import scala.util.Try

object Server extends StreamApp {


  override def stream(args: List[String]): Stream[Task, Nothing] = {

    Logger.getLogger("org").setLevel(Level.WARN)

    val (host, port) = Try(args(0))
      .map(_.split(":"))
      .flatMap(x => Try((x(0), x(1).toInt)))
      .getOrElse("localhost", 8080)

    val modelPath = Try(args(1)).getOrElse("data/model/asl_0")
    val repeatPurchaseModelPath = Try(args(2)).getOrElse("data/model/repeat_buy_1")
    val userLogPath = Try(args(3)).getOrElse("data/user_log_format1.csv")

    case class UserResponse(userIds: Seq[Int])
    case class ItemResponse(itemIds: Seq[Int])

    val predictModelService = new PredictModelService(
      modelPath,
      repeatPurchaseModelPath,
      userLogPath
    )

    val predictService = HttpService {
      case GET -> Root / "predictItemsByUserId" / IntVar(userId) / IntVar(
      limit) =>
        Ok(UserResponse(predictModelService.predictItemsByUserId(userId, limit)).asJson)
      case GET -> Root / "predictUsersByItemId" / IntVar(itemId) / IntVar(
      limit) =>
        Ok(ItemResponse(predictModelService.predictUsersByItemId(itemId, limit)).asJson)
      case GET -> Root / "predictRepeatableBuy" / IntVar(sellerId) / IntVar(
      limit) =>
        Ok(UserResponse(predictModelService.predictRepeatableBuy(sellerId, limit)).asJson)
    }


    BlazeBuilder
      .bindHttp(port, host)
      .mountService(predictService, "/")
      .serve
  }

}

package core

import java.time.LocalDate

import org.apache.spark.SparkConf
import org.apache.spark.ml.classification.GBTClassifier
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.mllib.recommendation.{ALS => ALSl}
import org.apache.spark.sql.SparkSession

import scala.util.Try



object PredictionModelBuildJob extends App
{
  val userLogPath = Try(args(0)).getOrElse("data/user_log_format1.csv")
  val modelPath = Try(args(1)).getOrElse("data/model/user_item")
  val fromDate = Try(LocalDate.parse(args(2))).getOrElse(LocalDate.now())
  val toDate = Try(LocalDate.parse(args(3))).getOrElse(LocalDate.now().plusDays(2))

  import DataStructs._

  val conf = new SparkConf()
    .setAppName("bit-se")
    .setMaster("local[3]")

  implicit val spark: SparkSession = SparkSession
    .builder()
    .config(conf)
    .getOrCreate()

  import spark.implicits._

  val userLog0 = userLog(userLogPath)
    .persist()

//  val fromDate = date
//  val toDate = date
//  //  val toDate = LocalDate.parse("2015-10-30")

  val features = extractUserItemFeature(userLog0, fromDate, toDate)


  val splits = features.randomSplit(Array[Double](0.7, 0.3), 42)
  val train = splits(0)
  val test = splits(1)



  val positiveClass = train
    .where($"has_bought" === 1)
    .persist()

  val negativeClass = train
    .where($"has_bought" === 0)



  val positiveClassSample = positiveClass
    .sample(false,negativeClass.count() / positiveClass.count() * 0.25 , 42)
    .persist()

  val balancedRawFeatures = positiveClassSample union negativeClass


  val c = new GBTClassifier()
    .setStepSize(0.1)
    .setSubsamplingRate(0.8)
    .setMaxIter(20)
    .setMaxDepth(13)
    .setMinInstancesPerNode(1800)
    .setMaxBins(4)
    .setFeaturesCol("features")
    .setLabelCol("has_bought")


  val m = c.fit(train)

  m.save(modelPath)


  val predictions = m.transform(test)
  val evaluator = new MulticlassClassificationEvaluator()
    .setPredictionCol("prediction")
    .setLabelCol("has_bought")
    .setMetricName("f1")
  val f1 = evaluator.evaluate(predictions)

  println(s"F1 =  ${BigDecimal(f1).setScale(4, BigDecimal.RoundingMode.UP)} ")

}

package core

import java.time.LocalDate

import org.apache.spark.SparkConf
import org.apache.spark.ml.linalg.DenseVector
import org.apache.spark.mllib.recommendation.{Rating, ALS => ALSl}
import org.apache.spark.sql.SparkSession

import scala.util.Try

object AtlModelBuildJob extends App {

  val userLogPath = Try(args(0)).getOrElse("data/user_log_format1.csv")
  val date = Try(LocalDate.parse(args(1))).getOrElse(LocalDate.now())
  val modelPath = Try(args(2)).getOrElse("data/model/asl")

  import DataStructs._

  val conf = new SparkConf()
    .setAppName("bit-se")
    .setMaster("local[3]")

  implicit val spark: SparkSession = SparkSession
    .builder()
    .config(conf)
    .getOrCreate()

  import spark.implicits._

  val userLog0 = userLog(userLogPath)
    .persist()

  val fromDate = date
  val toDate = date
//  val toDate = LocalDate.parse("2015-10-30")

  val features = extractUserItemFeature(userLog0, fromDate, toDate)

  val m = loadGBDTModel("data/model/user_item_2")

  val predictions = m.transform(features)

  val ratings = computeUserItemScores(userLog0, toDate)
    .join(predictions, Seq("user_id", "item_id"), "left")
    .where($"prediction".isNotNull)
    .map(
      x =>
        Rating(x.getAs[Long]("user_id").toInt,
               x.getAs[Long]("item_id").toInt,
               x.getAs[DenseVector]("probability")(1) * x.getAs[Double](
                 "total_decayed_score")))

  ALSl
    .train(ratings.rdd, 10, 10, 0.01)
    .save(spark.sparkContext, modelPath)

  println("Done")

}

package core

import java.sql.Timestamp
import java.time.LocalDate

import org.apache.spark.SparkConf
import org.apache.spark.ml.classification.GBTClassifier
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions._
import org.apache.spark.sql.functions._

import scala.util.Try

object RepeatBuyModelJob extends App {


  val userLogPath = Try(args(0)).getOrElse("data/user_log_format1.csv")
  val date = Try(LocalDate.parse(args(1))).getOrElse(LocalDate.parse("2015-10-30"))
  val modelPath = Try(args(2)).getOrElse("data/model/repeat_buy_new")

  import DataStructs._

  val conf = new SparkConf()
    .setAppName("bit-se")
    .setMaster("local[3]")

  implicit val spark: SparkSession = SparkSession
    .builder()
    .config(conf)
    .getOrCreate()

  import spark.implicits._

  val userLog0 = userLog(userLogPath)
    .persist()

  val fromDate = date.minusDays(10)
  //  val toDate = LocalDate.parse("2015-10-30")
  val toDate = date

  val ts1 = Timestamp.valueOf(fromDate.format(tsFormatter))
  val ts2 = Timestamp.valueOf(toDate.format(tsFormatter))
//
//  extractRepeatPurchaseSellerRawFeatures(
//    userLog0,
//    fromDate,
//    toDate
//  )
//
//  .sort("user_id", "seller_id")
//      .show(100, false)
//
//  sys.exit()
//  .repartition(1)
//  .write
//  .option("header", "true")
//  .csv("data/repeat_buy.csv")
//  sys.exit()

  val rawFeatures = extractRepeatPurchaseSellerFeatures(
    userLog0,
    fromDate,
    toDate
  )

  val splits = rawFeatures.randomSplit(Array[Double](0.7, 0.3), 42)
  val train = splits(0)
  val test = splits(1)

  val positiveClass = train
    .where($"has_bought" === 1)
    .persist()

  val negativeClass = train
    .where($"has_bought" === 0)



  val positiveClassSample = positiveClass
    .sample(false,negativeClass.count() / positiveClass.count() * 0.25 , 42)
    .persist()

  val balancedRawFeatures = positiveClassSample union negativeClass



  val c = new GBTClassifier()
//    .setStepSize(0.1)
//    .setSubsamplingRate(0.8)
//    .setMaxDepth(5)
//    .setMaxIter(60)
//    .setMinInstancesPerNode(1400)
//    .setMaxBins(2)
    .setFeaturesCol("features")
    .setLabelCol("has_bought")


  val m = c.fit(train)

  m.save(modelPath)


  val predictions = m.transform(test)
  val evaluator = new MulticlassClassificationEvaluator()
    .setPredictionCol("prediction")
    .setLabelCol("has_bought")
    .setMetricName("f1")
  val f1 = evaluator.evaluate(predictions)

  println(s"F1 =  ${BigDecimal(f1).setScale(4, BigDecimal.RoundingMode.UP)} ")

}

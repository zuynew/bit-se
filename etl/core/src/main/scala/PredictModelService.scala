package core

import java.time.LocalDate

import org.apache.spark.SparkConf
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.mllib.linalg.DenseVector
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions._
import org.apache.spark.sql.functions._

import scala.util.Try

case class SellerFeatures(
    user_merchant_rank: Int,
    user_cat_merchant_rank: Int,
    total_user_interaction: Int,
    total_user_merchant_interaction: Int,
    purchase_count: Int,
    user_merchant_interaction_ratio: Double
)

class PredictModelService(alsModelPath: String,
                          repeatPurchaseModelPath: String, userLogPath: String) {

  val conf = new SparkConf()
    .setAppName("bit-se")
    .setMaster("local[3]")

  implicit val spark: SparkSession = SparkSession
    .builder()
    .config(conf)
    .getOrCreate()

  import spark.implicits._

  import DataStructs._

  val userLog0 = userLog(userLogPath).persist()

  private lazy val alsModel = loadALSTModel(alsModelPath)
  private lazy val repeatPurchaseModel = loadGBDTModel(repeatPurchaseModelPath)
    .setFeaturesCol("features")
    .setPredictionCol("prediction")
    .setProbabilityCol("probability")

  def predictItemsByUserId(userId: Int, limit: Int = 10): List[Int] =
    Try(alsModel.recommendProducts(userId, limit))
      .map(_.map(_.product).toList)
      .getOrElse(Nil)

  def predictUsersByItemId(itemId: Int, limit: Int = 10): List[Int] =
    Try(alsModel.recommendUsers(itemId, limit))
      .map(_.map(_.user).toList)
      .getOrElse(Nil)

  def predictRepeatableBuy(sellerId: Int,
                           limit: Int = 10): List[Int] = {
    val date = LocalDate.parse("2015-10-30").minusDays(1)
    val features = extractRepeatPurchaseSellerFeatures(
      userLog0,
      date.minusDays(7),
      date,
      Option(sellerId)
    )
    .select("user_id", "seller_id", "features")

    repeatPurchaseModel
      .transform(features)
      .map(x =>
        (x.getAs[Int]("user_id"), x.getAs[DenseVector]("probability")(1))
      )
      .rdd
      .sortBy(-_._2)
      .map(_._1)
      .take(limit)
      .toList
  }

}

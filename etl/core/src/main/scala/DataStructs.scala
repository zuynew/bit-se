package core

import java.sql.Timestamp
import java.time.{LocalDate, Period, ZoneId}
import java.time.format.DateTimeFormatter

import org.apache.spark.ml.classification.{
  GBTClassificationModel,
  GBTClassifier
}
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.ml.recommendation.ALS
import org.apache.spark.mllib.recommendation.MatrixFactorizationModel
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Column, ColumnName, DataFrame, SparkSession}
import org.apache.spark.sql.types._

object DataStructs {

  val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd")
  val tsFormatter = DateTimeFormatter.ofPattern("yyy-MM-dd '00:00:00'")
  val mmddFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("MMdd")
  val zoneId: ZoneId = ZoneId.systemDefault()

  val userLogCsvSchema: StructType = new StructType()
    .add("user_id", LongType, nullable = false)
    .add("item_id", LongType, nullable = false)
    .add("cat_id", LongType, nullable = false)
    .add("seller_id", LongType, nullable = false)
    .add("brand_id", LongType, nullable = false)
    .add("time_stamp", StringType, nullable = false)
    .add("action_type", StringType, nullable = false)

  val userInfoCsvSchema: StructType = new StructType()
    .add("user_id", LongType, nullable = false)
    .add("age_range", StringType, nullable = false)
    .add("gender", StringType, nullable = false)

  def userLog(filePath: String)(implicit spark: SparkSession): DataFrame = {
    import spark.implicits._
    import org.apache.spark.sql.functions._

    spark.read
      .schema(userLogCsvSchema)
      .option("header", "true")
      .csv(filePath)
      .withColumn("time_stamp_ts", udf[Timestamp, String]((x: String) => {
        val (m, d) = x.splitAt(2)
        Timestamp.valueOf(s"2015-${m}-${d} 00:00:00")
      }).apply($"time_stamp"))
  }

  def userInfo(filePath: String)(implicit spark: SparkSession): DataFrame = {
    spark.read
      .schema(userInfoCsvSchema)
      .csv(filePath)
  }

  def computeBuyRatio(userLog: DataFrame,
                      fromDate: LocalDate,
                      toDate: LocalDate,
                      length: Int = 7,
                      groupBy: Seq[Column] = Seq(new ColumnName("user_id")))(
      implicit spark: SparkSession): DataFrame = {

    import spark.implicits._

    val ts1 = Timestamp.valueOf(fromDate.format(tsFormatter))

    val tsLogs = for (i <- 0 to Period.between(fromDate, toDate).getDays)
      yield {
        val currentDate = fromDate.plusDays(i).format(mmddFormatter)
        userLog
          .where(
            ($"time_stamp_ts" - expr(s"INTERVAL ${i} DAY") < ts1)
              and ($"time_stamp_ts" - expr(s"INTERVAL ${i} DAY") + expr(
                s"INTERVAL ${length} DAY") >= ts1)
          )
          .withColumn("is_buy", when($"action_type" === "2", 1).otherwise(0))
          .withColumn("is_action", expr("1"))
          .groupBy(groupBy :+ $"time_stamp": _*)
          .agg(sum("is_action").as("action_count"),
               sum("is_buy").as("buy_count"))
//          .sum("is_action", "is_buy")
//          .withColumnRenamed("sum(is_action)", "action_count")
//          .withColumnRenamed("sum(is_buy)", "buy_count")
          .withColumn("has_action", when($"action_count" > 0, 1).otherwise(0))
          .withColumn("has_buy", when($"buy_count" > 0, 1).otherwise(0))
          .withColumn("time_stamp", expr(s"'${currentDate}'"))
          .groupBy(groupBy :+ $"time_stamp": _*)
          .agg(sum("has_action").as("action_count"),
               sum("has_buy").as("buy_count"))
//          .sum("has_action", "has_buy")
//          .withColumnRenamed("sum(has_action)", "action_count")
//          .withColumnRenamed("sum(has_buy)", "buy_count")
          .withColumn("action_prob", $"action_count" / length.toFloat)
          .withColumn("buy_prob", $"buy_count" / length.toFloat)
          .withColumn("active_buy_ratio", $"buy_count" / $"action_count")
      }

    val (head :: tail) = tsLogs.toList

    tail.fold(head)((acc: DataFrame, df: DataFrame) => acc.union(df))

  }

  def decay(lambda: Float)(c: Column): Column = pow(lambda, log(pow(c, 2) + 1))

  def computeDecayedBehaviorCount(userLog: DataFrame,
                                  fromDate: LocalDate,
                                  toDate: LocalDate,
                                  length: Int = 30,
                                  lambda: Float = 0.7F,
                                  groupBy: Seq[Column] = Seq(
                                    new ColumnName("user_id")))(
      implicit spark: SparkSession): DataFrame = {

    import spark.implicits._

    val ts1 = Timestamp.valueOf(fromDate.format(tsFormatter))
    val ts2 = Timestamp.valueOf(toDate.format(tsFormatter))

    val tsLogs = for (i <- 0 to Period.between(fromDate, toDate).getDays)
      yield {
        val currentDate = fromDate.plusDays(i)
        val currentDateMMDD = currentDate.format(mmddFormatter)
        userLog
          .where(
            ($"time_stamp_ts" < ts1)
              and
                ($"time_stamp_ts" + expr(s"INTERVAL ${length} DAY") >= ts1)
          )
          .withColumn("day_diff",
                      datediff(expr(s"'${currentDate.format(tsFormatter)}'"),
                               $"time_stamp_ts"))
          .withColumn("action_score", expr("1"))
          .withColumn("decay", decay(lambda)($"day_diff"))
          .withColumn("time_stamp", expr(s"'${currentDateMMDD}'"))
          .groupBy(groupBy :+ $"time_stamp": _*)
          .agg(
            sum("decay").as("decayed_behavior_count"),
            sum("action_score").as("total_behavior_count")
          )
      }

    val (head :: tail) = tsLogs.toList

    tail.fold(head)((acc: DataFrame, df: DataFrame) => acc.union(df))

  }

  def computeUserCategoryBehaviorCount(
      userLog: DataFrame,
      fromDate: LocalDate,
      toDate: LocalDate,
      length: Int = 30)(implicit spark: SparkSession): DataFrame = {

    import spark.implicits._

    val ts1 = Timestamp.valueOf(fromDate.format(tsFormatter))
    val ts2 = Timestamp.valueOf(toDate.format(tsFormatter))

    val tsLogs = for (i <- 0 to Period.between(fromDate, toDate).getDays)
      yield {
        val currentDate = fromDate.plusDays(i)
        val currentDateMMDD = currentDate.format(mmddFormatter)
        userLog
          .where(
            ($"time_stamp_ts" < ts1)
              and
                ($"time_stamp_ts" + expr(s"INTERVAL ${length} DAY") >= ts1)
          )
          .withColumn("time_stamp", expr(s"'${currentDateMMDD}'"))
          .withColumn("action_score", expr("1"))
          .groupBy($"user_id", $"cat_id", $"time_stamp")
          .agg(sum("action_score").as("user_category_behavior_count"))
      }

    val (head :: tail) = tsLogs.toList

    tail.fold(head)((acc: DataFrame, df: DataFrame) => acc.union(df))

  }

  def computeUserItemBehaviorCount(
      userLog: DataFrame,
      fromDate: LocalDate,
      toDate: LocalDate,
      length: Int = 30)(implicit spark: SparkSession): DataFrame = {

    import spark.implicits._

    val ts1 = Timestamp.valueOf(fromDate.format(tsFormatter))
    val ts2 = Timestamp.valueOf(toDate.format(tsFormatter))

    val tsLogs = for (i <- 0 to Period.between(fromDate, toDate).getDays)
      yield {
        val currentDate = fromDate.plusDays(i)
        val currentDateMMDD = currentDate.format(mmddFormatter)
        userLog
          .where(
            ($"time_stamp_ts" < ts1)
              and
                ($"time_stamp_ts" + expr(s"INTERVAL ${length} DAY") >= ts1)
          )
          .withColumn("time_stamp", expr(s"'${currentDateMMDD}'"))
          .withColumn("action_score", expr("1"))
          .groupBy($"user_id", $"item_id", $"time_stamp")
          .agg(sum("action_score").as("user_item_behavior_count"))
      }

    val (head :: tail) = tsLogs.toList

    tail.fold(head)((acc: DataFrame, df: DataFrame) => acc.union(df))

  }

  def computeUserItemBehaviorRank(
      userLog: DataFrame,
      fromDate: LocalDate,
      toDate: LocalDate,
      length: Int = 30)(implicit spark: SparkSession): DataFrame = {

    import spark.implicits._

    val ts1 = Timestamp.valueOf(fromDate.format(tsFormatter))
    val ts2 = Timestamp.valueOf(toDate.format(tsFormatter))

    val tsLogs = for (i <- 0 to Period.between(fromDate, toDate).getDays)
      yield {
        val currentDate = fromDate.plusDays(i)
        val currentDateMMDD = currentDate.format(mmddFormatter)
        userLog
          .where(
            ($"time_stamp_ts" < ts1)
              and
                ($"time_stamp_ts" + expr(s"INTERVAL ${length} DAY") >= ts1)
          )
          .withColumn("action_score", expr("1"))
          .withColumn("total_user_interaction",
                      sum("action_score")
                        .over(
                          Window
                            .partitionBy("user_id")
                        ))
          .withColumn("total_item_interaction",
                      sum("action_score")
                        .over(
                          Window
                            .partitionBy("item_id")
                        ))
          .withColumn("total_user_item_interaction",
                      sum("action_score")
                        .over(
                          Window
                            .partitionBy("user_id", "item_id")
                        ))
          .withColumn("total_user_cat_interaction",
                      sum("action_score")
                        .over(
                          Window
                            .partitionBy("user_id", "cat_id")
                        ))
          .withColumn(
            "user_item_rank",
            dense_rank().over(
              Window
                .partitionBy("user_id")
                .orderBy($"total_user_item_interaction".desc_nulls_last)
            ))
          .withColumn(
            "user_item_in_cat_rank",
            dense_rank().over(
              Window
                .partitionBy("user_id", "cat_id")
                .orderBy($"total_user_item_interaction".desc_nulls_last)
            ))
          .withColumn("item_in_cat_rank",
                      dense_rank().over(
                        Window
                          .partitionBy("cat_id")
                          .orderBy($"total_item_interaction".desc_nulls_last)
                      ))
          .withColumn(
            "user_cat_in_user_rank",
            dense_rank().over(
              Window
                .partitionBy("user_id")
                .orderBy($"total_user_cat_interaction".desc_nulls_last)
            ))
          .select("user_id",
                  "item_id",
                  "cat_id",
                  "user_cat_in_user_rank",
                  "user_item_rank",
                  "user_item_in_cat_rank",
                  "item_in_cat_rank")
          .distinct()
          .withColumn("time_stamp", expr(s"'${currentDateMMDD}'"))
      }

    val (head :: tail) = tsLogs.toList

    tail.fold(head)((acc: DataFrame, df: DataFrame) => acc.union(df))

  }

  def computeNextDayProb(userLog: DataFrame,
                         fromDate: LocalDate,
                         toDate: LocalDate,
                         length: Int = 30,
                         groupBy: Seq[String] = Seq("user_id"))(
      implicit spark: SparkSession): DataFrame = {

    import spark.implicits._

    val groupByColumn = groupBy.map(new ColumnName(_))

    val ts1 = Timestamp.valueOf(fromDate.format(tsFormatter))

    val directBuyLog = userLog
      .where($"action_type" === "2")
      .groupBy("user_id", "item_id", "time_stamp")
      .count()
      .withColumnRenamed("count", "direct_buy_count")
      .withColumn("has_bought", when($"direct_buy_count" > 0, 1).otherwise(0))
      .persist()

    val tsLogs = for (i <- 0 to Period.between(fromDate, toDate).getDays)
      yield {
        val currentDate = fromDate.plusDays(i).format(mmddFormatter)
        directBuyLog
          .withColumn("time_stamp_ts", udf[Timestamp, String](x => {
            val (m, d) = x.splitAt(2)
            Timestamp.valueOf(s"2015-${m}-${d} 00:00:00")
          }).apply($"time_stamp"))
          .withColumn("next_time_stamp", udf((s: Timestamp) => {
            s.toLocalDateTime
              .plusDays(1)
              .format(DateTimeFormatter.ofPattern("MMdd"))
          }).apply($"time_stamp_ts"))
          .where(
            ($"time_stamp_ts" - expr(s"INTERVAL ${i} DAY") < ts1)
              and ($"time_stamp_ts" - expr(s"INTERVAL ${i} DAY") + expr(
                s"INTERVAL ${length} DAY") >= ts1)
          )
          .alias("n")
          .join(
            directBuyLog
              .alias("d")
              .withColumnRenamed("has_bought", "next_day_buy_count"),
            $"d.user_id" === $"n.user_id"
              and $"d.item_id" === $"n.item_id"
              and $"d.time_stamp" === $"n.next_time_stamp",
            "left"
          )
          .select(((groupBy :+ "direct_buy_count")
            .map({ "n." + _ }) :+ "next_day_buy_count")
            .map(new ColumnName(_)): _*)
          .withColumn("time_stamp", expr(s"'${currentDate}'"))
          .groupBy(groupByColumn :+ $"time_stamp": _*)
          .agg(sum("direct_buy_count").as("direct_buy_count"),
               sum("next_day_buy_count").as("next_day_buy_count"))
//          .sum("direct_buy_count", "next_day_buy_count")
//          .withColumnRenamed("sum(direct_buy_count)", "direct_buy_count")
//          .withColumnRenamed("sum(next_day_buy_count)", "next_day_buy_count")
          .withColumn("next_day_prob",
                      $"next_day_buy_count" / $"direct_buy_count")
      }

    val (head :: tail) = tsLogs.toList

    tail.fold(head)((acc: DataFrame, df: DataFrame) => acc.union(df))
  }

  def extractFeature(userLog: DataFrame,
                     fromDate: LocalDate,
                     toDate: LocalDate,
                     groupBy: Seq[String] = Seq("user_id"))(
      implicit spark: SparkSession): DataFrame = {

    import spark.implicits._

    val groupByColumns = groupBy.map(new ColumnName(_))

    val ts1 = Timestamp.valueOf(fromDate.format(tsFormatter))
    val ts2 = Timestamp.valueOf(toDate.format(tsFormatter))

    val userBuys = userLog
      .where(
        $"action_type" === "2"
          and $"time_stamp_ts" >= ts1
          and $"time_stamp_ts" <= ts2
      )
      .select(groupByColumns :+ $"time_stamp": _*)
      .distinct()
      .withColumn("has_bought", expr("1"))

    computeBuyRatio(
      userLog,
      fromDate,
      toDate,
      groupBy = groupByColumns
    ).join(userBuys, groupBy :+ "time_stamp", "left")
      .join(
        computeDecayedBehaviorCount(
          userLog,
          fromDate,
          toDate,
          groupBy = groupByColumns
        ),
        groupBy :+ "time_stamp",
        "left"
      )
      .join(
        computeNextDayProb(
          userLog,
          fromDate,
          toDate,
          groupBy = groupBy
        ),
        groupBy :+ "time_stamp",
        "left"
      )
      .withColumn("has_bought", when($"has_bought".isNull, 0).otherwise(1))
      .withColumn("next_day_buy_count",
                  coalesce($"next_day_buy_count", expr("0")))
      .withColumn("direct_buy_count", coalesce($"direct_buy_count", expr("0")))
      .withColumn("next_day_prob", coalesce($"next_day_prob", expr("0")))
      .withColumn("decayed_behavior_count",
                  coalesce($"decayed_behavior_count", expr("0")))
  }

  def extractUserItemRawFeature(
      userLog: DataFrame,
      fromDate: LocalDate,
      toDate: LocalDate)(implicit spark: SparkSession): DataFrame = {

    import spark.implicits._

    val ts1 = Timestamp.valueOf(fromDate.format(tsFormatter))
    val ts2 = Timestamp.valueOf(toDate.format(tsFormatter))

    val userBuys = userLog
      .where(
        $"time_stamp_ts" >= ts1
          and $"time_stamp_ts" <= ts2
      )
      .withColumn("action_score", when($"action_type" === "2", 1).otherwise(0))
      .groupBy($"user_id", $"item_id", $"time_stamp")
      .agg(sum("action_score").as("has_bought"))
      .withColumn("has_bought", when($"has_bought" > 0, 1).otherwise(0))

    computeDecayedBehaviorCount(
      userLog,
      fromDate,
      toDate,
      length = 35,
      lambda = 0.7F,
      groupBy = Seq($"user_id", $"item_id")
    ).join(
        computeUserItemBehaviorRank(userLog, fromDate, toDate, 7),
        Seq("user_id", "item_id", "time_stamp"),
        "left"
      )
      .join(
        userBuys,
        Seq("user_id", "item_id", "time_stamp"),
        "left"
      )
      .where($"decayed_behavior_count".isNotNull)
      .withColumn("has_bought", coalesce($"has_bought", expr("0")))
      .withColumn("total_behavior_count",
                  coalesce($"total_behavior_count", expr("0")))
      .withColumn("decayed_behavior_count",
                  coalesce($"decayed_behavior_count", expr("0")))
      .withColumn("user_cat_in_user_rank",
                  coalesce($"user_cat_in_user_rank", expr("0")))
      .withColumn("user_item_rank", coalesce($"user_item_rank", expr("0")))
      .withColumn("user_item_in_cat_rank",
                  coalesce($"user_item_in_cat_rank", expr("0")))
      .withColumn("item_in_cat_rank", coalesce($"item_in_cat_rank", expr("0")))
  }

  def extractUserItemFeature(
      userLog: DataFrame,
      fromDate: LocalDate,
      toDate: LocalDate)(implicit spark: SparkSession): DataFrame = {

    val assembler1 = new VectorAssembler()
      .setInputCols(
        Array(
          "user_cat_in_user_rank",
          "user_item_rank",
          "user_item_in_cat_rank",
          "item_in_cat_rank",
          "decayed_behavior_count",
          "total_behavior_count"
        ))
      .setOutputCol("features")

    assembler1
      .transform(extractUserItemRawFeature(userLog, fromDate, toDate))
      .select("user_id", "item_id", "features", "has_bought")
  }

  def extractRepeatPurchaseSellerRawFeatures(
      userLog: DataFrame,
      fromDate: LocalDate,
      toDate: LocalDate,
      sellerId: Option[Int] = None
  )(implicit spark: SparkSession): DataFrame = {

    import spark.implicits._

    val ts1 = Timestamp.valueOf(fromDate.format(tsFormatter))
    val ts2 = Timestamp.valueOf(toDate.format(tsFormatter))

    val purchases =
      userLog
        .where(
          $"action_type" === 2
            and $"time_stamp_ts" > ts2
            and $"time_stamp_ts" - expr("INTERVAL 1 week") <= ts2
            and sellerId.map($"seller_id" === _).getOrElse(expr("true"))
        )
        .groupBy("user_id", "seller_id")
        .agg(count("*").as("buy_count"))

    userLog
      .where(
        $"time_stamp_ts" >= ts1
          and $"time_stamp_ts" <= ts2
          and sellerId.map($"seller_id" === _).getOrElse(expr("true"))
      )
      .withColumn("action_score", expr("1"))

      ///////
      ////////
      .withColumn("total_user_interaction",
                  sum("action_score")
                    .over(
                      Window
                        .partitionBy("user_id")
                    ))
      .withColumn("total_user_purchase_count",
                  sum(when($"action_type" === 2, 1).otherwise(0))
                    .over(
                      Window
                        .partitionBy("user_id")
                    ))
      ////////
      /////////
      .withColumn("total_user_merchant_interaction",
                  sum("action_score")
                    .over(
                      Window
                        .partitionBy("user_id", "seller_id")
                    ))
      .withColumn("total_user_merchant_purchase_count",
                  sum(when($"action_type" === 2, 1).otherwise(0))
                    .over(
                      Window
                        .partitionBy("user_id", "seller_id")
                    ))
//      .withColumn("total_user_cat_interaction",
//        sum("action_score")
//          .over(
//            Window
//              .partitionBy("user_id", "cat_id")
//          ))
//      .withColumn("total_user_cat_purchase_count",
//        sum(when($"action_type" === 2, 1).otherwise(0))
//          .over(
//            Window
//              .partitionBy("user_id", "cat_id")
//          ))
//
//
//      .withColumn("total_user_brand_interaction",
//        sum("action_score")
//          .over(
//            Window
//              .partitionBy("user_id", "brand_id")
//          ))
//      .withColumn("total_user_brand_purchase_count",
//        sum(when($"action_type" === 2, 1).otherwise(0))
//          .over(
//            Window
//              .partitionBy("user_id", "brand_id")
//          ))
      .withColumn("total_user_cat_merchant_interaction",
                  sum("action_score")
                    .over(
                      Window
                        .partitionBy("user_id", "cat_id", "seller_id")
                    ))
      .withColumn("total_user_cat_merchant_purchase_count",
                  sum(when($"action_type" === 2, 1).otherwise(0))
                    .over(
                      Window
                        .partitionBy("user_id", "cat_id", "seller_id")
                    ))
      ////////
      /////////
      .withColumn("total_user_brand_merchant_interaction",
                  sum("action_score")
                    .over(
                      Window
                        .partitionBy("user_id", "brand_id", "seller_id")
                    ))
      .withColumn("total_user_brand_merchant_purchase_count",
                  sum(when($"action_type" === 2, 1).otherwise(0))
                    .over(
                      Window
                        .partitionBy("user_id", "brand_id", "seller_id")
                    ))
      ////////
      /////////
      .withColumn(
        "user_merchant_rank",
        dense_rank().over(
          Window
            .partitionBy("user_id")
            .orderBy($"total_user_merchant_interaction".asc_nulls_last)
        ))
      .withColumn(
        "user_cat_merchant_rank",
        dense_rank().over(
          Window
            .partitionBy("user_id")
            .orderBy($"total_user_cat_merchant_interaction".asc_nulls_last)
        ))
      .withColumn(
        "user_brand_merchant_rank",
        dense_rank().over(
          Window
            .partitionBy("user_id")
            .orderBy($"total_user_brand_merchant_interaction".asc_nulls_last)
        ))
      ////////
      /////////
      .withColumn(
        "user_merchant_purchase_rank",
        dense_rank().over(
          Window
            .partitionBy("user_id")
            .orderBy($"total_user_merchant_purchase_count".asc_nulls_last)
        ))
      .withColumn(
        "user_cat_merchant_purchase_rank",
        dense_rank().over(
          Window
            .partitionBy("user_id")
            .orderBy($"total_user_cat_merchant_purchase_count".asc_nulls_last)
        ))
      .withColumn(
        "user_brand_merchant_purchase_rank",
        dense_rank().over(
          Window
            .partitionBy("user_id")
            .orderBy($"total_user_brand_merchant_purchase_count".asc_nulls_last)
        )
      )
      .select(
        "user_id",
        "seller_id",
//        "time_stamp",
        "total_user_interaction",
        "total_user_purchase_count",
        "total_user_merchant_interaction",
        "total_user_merchant_purchase_count",
        "total_user_cat_merchant_interaction",
        "total_user_cat_merchant_purchase_count",
        "total_user_brand_merchant_interaction",
        "total_user_brand_merchant_purchase_count",
        "user_merchant_rank",
        "user_cat_merchant_rank",
        "user_brand_merchant_rank",
        "user_merchant_purchase_rank",
        "user_cat_merchant_purchase_rank",
        "user_brand_merchant_purchase_rank"
      )
      .groupBy(
        "user_id",
        "seller_id"
      )
      .agg(
        max($"total_user_interaction").as("total_user_interaction"),
        max($"total_user_purchase_count").as("total_user_purchase_count"),
        max($"total_user_merchant_interaction").as(
          "total_user_merchant_interaction"),
        max($"total_user_merchant_purchase_count").as(
          "total_user_merchant_purchase_count"),
        max($"total_user_cat_merchant_interaction").as(
          "total_user_cat_merchant_interaction"),
        max($"total_user_cat_merchant_purchase_count").as(
          "total_user_cat_merchant_purchase_count"),
        max($"total_user_brand_merchant_interaction").as(
          "total_user_brand_merchant_interaction"),
        max($"total_user_brand_merchant_purchase_count").as(
          "total_user_brand_merchant_purchase_count"),
        max($"user_merchant_rank").as("user_merchant_rank"),
        max($"user_cat_merchant_rank").as("user_cat_merchant_rank"),
        max($"user_brand_merchant_rank").as("user_brand_merchant_rank"),
        max($"user_merchant_purchase_rank").as("user_merchant_purchase_rank"),
        max($"user_cat_merchant_purchase_rank").as(
          "user_cat_merchant_purchase_rank"),
        max($"user_brand_merchant_purchase_rank").as(
          "user_brand_merchant_purchase_rank")
      )
      .distinct()
      .withColumn(
        "user_merchant_interaction_ratio",
        $"total_user_merchant_interaction" / $"total_user_interaction"
      )
      .withColumn(
        "user_merchant_ctr",
        $"total_user_merchant_purchase_count" / $"total_user_interaction"
      )
      .withColumn(
        "user_ctr",
        $"total_user_purchase_count" / $"total_user_interaction"
      )
      .join(purchases, Seq("user_id", "seller_id"), "left")
      .withColumn(
        "has_bought",
        when(coalesce($"buy_count", expr("0")) > 0, 1).otherwise(0)
      )
      .drop("buy_count")

  }

  def extractRepeatPurchaseSellerFeatures(
      userLog: DataFrame,
      fromDate: LocalDate,
      toDate: LocalDate,
      sellerId: Option[Int] = None)(implicit spark: SparkSession): DataFrame = {

    val assembler1 = new VectorAssembler()
      .setInputCols(Array(
        "total_user_interaction",
        "total_user_purchase_count",
        "total_user_merchant_interaction",
        "total_user_merchant_purchase_count",
        "total_user_cat_merchant_interaction",
        "total_user_cat_merchant_purchase_count",
        "total_user_brand_merchant_interaction",
        "total_user_brand_merchant_purchase_count",
        "user_merchant_rank",
        "user_cat_merchant_rank",
        "user_brand_merchant_rank",
        "user_merchant_purchase_rank",
        "user_cat_merchant_purchase_rank",
        "user_brand_merchant_purchase_rank",
        "user_merchant_interaction_ratio",
        "user_merchant_ctr",
        "user_ctr"
      ))
      .setOutputCol("features")

    assembler1
      .transform(
        extractRepeatPurchaseSellerRawFeatures(userLog,
                                               fromDate,
                                               toDate,
                                               sellerId))
      .select("user_id", "seller_id", "features", "has_bought")
  }

  def loadGBDTModel(path: String): GBTClassificationModel = {
    GBTClassificationModel.load(path)
  }

  def loadALSTModel(path: String)(
      implicit spark: SparkSession): MatrixFactorizationModel = {
    MatrixFactorizationModel.load(spark.sparkContext, path)
  }

  def computeUserItemScores(
      userLog: DataFrame,
      toDate: LocalDate,
      length: Int = 30,
      lambda: Float = 0.7F)(implicit spark: SparkSession): DataFrame = {

    import spark.implicits._

    val ts1 = Timestamp.valueOf(toDate.format(tsFormatter))

    userLog
      .where(
        $"time_stamp_ts" <= ts1
          and ($"time_stamp_ts" + expr(s"INTERVAL ${length} DAY") > ts1)
      )
      .withColumn(
        "action_score",
        udf[Int, Int]({
          case 0 => 1 // click
          case 1 => 2 // add to cart
          case 3 => 2 // add to fav
          case 2 => 3 // purchase
        }).apply($"action_type")
      )
      .withColumn("day_diff",
                  datediff(expr(s"'${toDate.format(tsFormatter)}'"),
                           $"time_stamp_ts"))
      .withColumn("decayed_score", $"action_score" * decay(lambda)($"day_diff"))
      .select("user_id", "item_id", "action_score", "decayed_score")
      .groupBy($"user_id", $"item_id")
      .agg(sum($"action_score").as("total_score"),
           sum($"decayed_score").as("total_decayed_score"))
  }
}

<?php


namespace Util\Semigroup;


interface ISemigroup
{
    public function combine(ISemigroup $routerGroupBuilderExpr): ISemigroup;
}
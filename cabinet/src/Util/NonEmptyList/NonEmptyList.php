<?php


namespace Util\NonEmptyList;


use Util\Semigroup\ISemigroup;

class NonEmptyList
{
    protected $_list;

    /**
     * NonEmptyList constructor.
     * @param $head
     * @param array $tail
     */
    public function __construct($head, ...$tail)
    {
        $this->_list = array_merge([$head], $tail);
    }

    public function fold(?callable $callable = null, $zero = null)
    {
        if (!isset($callable))
        {
            if ($this->_list[0] instanceof ISemigroup)
            {
                return array_reduce(
                    array_slice($this->_list, 1),
                    function (ISemigroup $acc, ISemigroup $item) {

                        return $acc->combine($item);
                    },
                    $this->_list[0]
                );
            } else {
                throw new \Exception("Only ISemigroup NEL could be reduced");
            }
        }
        elseif (isset($callable) && isset($zero))
        {

            return array_reduce(
                $this->_list,
                $callable,
                $zero
            );
        }
        else
        {
            throw new \Exception("Function and zero required");
        }

    }

}
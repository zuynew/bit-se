<?php


namespace Util;

class ArrayHelper
{


    static protected function _dig0(array $arr, array $chunks)
    {
        if (empty($chunks))
        {
            return $arr;
        }

        list($head, $tail) = static::headTail($chunks);

        if (isset($arr[$head]))
        {
            if (empty($tail))
            {
                return $arr[$head];
            }
            elseif (is_array($arr[$head]))
            {
                return static::_dig0($arr[$head], $tail);
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }
    
    static public function dig(array $arr, string $path)
    {
        return static::_dig0($arr, array_filter(explode('.', $path)));
    }

    static public function headTail(array $arr): array
    {
        list($head, ) = $arr;
        $tail = array_slice($arr, 1);

        return [$head, $tail];
    }
}
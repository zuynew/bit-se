<?php

namespace Util\Config;

use Util\ArrayHelper;

class TypedConfiguration
{

    /**
     * @var array
     */
    protected $_configArray;

    public function __construct(array $configArray)
    {
        $this->_configArray = $configArray;
    }

    public function getInt(string $path, int $default = null): int
    {
        $val = $this->_get($path, $default);
        if (!is_int($val))
        {
            throw new InvalidTypeException($path, "int");
        }
        
        return (int)$val;
    }
    
    public function getFloat(string $path, float $default = null): float
    {
        $val = $this->_get($path, $default);
        if (!is_float($val))
        {
            throw new InvalidTypeException($path, "float");
        }

        return (float)$val;
    }

    public function getBoolean(string $path, bool $default = null): bool 
    {
        $val = $this->_get($path, $default);
        if (!is_bool($val))
        {
            throw new InvalidTypeException($path, "bool");
        }

        return (bool)$val;
    }
    
    public function getString(string $path, string $default = null): string
    {
        $val = $this->_get($path, $default);
        if (!is_string($val))
        {
            throw new InvalidTypeException($path, "string");
        }

        return (string)$val;
    }
    
    public function getArray(string $path, array $default = null): array
    {
        $val = $this->_get($path, $default);
        if (!is_array($val))
        {
            throw new InvalidTypeException($path, "array");
        }

        return $val;
    }

    public function has(string $path): bool 
    {
        return !is_null(ArrayHelper::dig($this->_configArray, $path));
    }

    protected function _get(string $path, $default = null)
    {
        if ($this->has($path))
        {
            return ArrayHelper::dig($this->_configArray, $path);
        }
        elseif(isset($default))
        {
            return $default;
        }
        else
        {
            throw new ConfigPathUndefinedException($path);
        }
    }
}
<?php

namespace Util\Config;


class ConfigPathUndefinedException extends \Exception
{

    /**
     * ConfigPathUndefined constructor.
     * @param string $path
     */
    public function __construct(string $path)
    {
        parent::__construct("Path: $path is undefined");
    }
}
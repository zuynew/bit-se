<?php

namespace Util\Config;


class InvalidTypeException extends \Exception
{

    /**
     * ConfigPathUndefined constructor.
     * @param string $path
     * @param string $type
     */
    public function __construct(string $path, string $type)
    {
        parent::__construct("Path: $path is not type of $type");
    }
}
<?php

namespace Util\Config;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;


class AppConfiguration
{
    static public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder();
        $treeBuilder
            ->root('app')
                ->children()
                    ->booleanNode("is_dev")
                        ->defaultValue(false)
                    ->end()
                ->end()
                ->children()
                    ->arrayNode('db')
                        ->children()
                            ->scalarNode("host")->end()
                            ->integerNode("port")
                                ->defaultValue(5432)
                            ->end()
                            ->scalarNode("dbname")->end()
                            ->scalarNode("user")->end()
                            ->scalarNode("password")->end()
                        ->end()
                    ->end()
                ->end()
                ->children()
                    ->arrayNode("twig")
                        ->children()
                            ->scalarNode("cache_path")
                                ->defaultValue("/tmp/twig-cache")
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->children()
                    ->arrayNode("prediction_model_service")
                        ->children()
                            ->scalarNode("host")
                                ->defaultValue("localhost")
                            ->end()
                        ->end()
                        ->children()
                            ->integerNode("port")
                                ->defaultValue(80)
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()

        ;

        return $treeBuilder;
    }
}
<?php

namespace Util\Config;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Yaml\Yaml;

class ConfigFactory
{

    static public function load(string $configPath = null, TreeBuilder $configTreeBuilder = null): TypedConfiguration
    {
        $configArr = Yaml::parseFile($configPath ?? (ROOT_PATH . '/configs/config.yml'));

        if ($configTreeBuilder instanceof TreeBuilder)
        {
            $processor = new Processor();

            $configArr = $processor->process($configTreeBuilder->buildTree(), $configArr);
        }

        return new TypedConfiguration($configArr);
    }

}
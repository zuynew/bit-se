<?php


namespace Util\Auth\AuthIdentity;


interface IAuthIdentityWithPassword
    extends IAuthIdentity
{
    public function getPassword(): string;
}
<?php


namespace Util\Auth\AuthIdentity;


interface IAuthIdentity
{

    public function getId();
}
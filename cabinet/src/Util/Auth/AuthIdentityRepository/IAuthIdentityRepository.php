<?php


namespace Util\Auth\AuthIdentityRepository;


use Util\Auth\AuthIdentity\IAuthIdentity;

interface IAuthIdentityRepository
{


    /**
     * @param $id
     * @return IAuthIdentity
     */
    public function findIdentity($id);
}
<?php


namespace Util\Auth\AuthIdentityRepository;


use Util\Auth\AuthIdentity\IAuthIdentityWithPassword;

interface IAuthIdentityWithPasswordRepository
    extends IAuthIdentityRepository
{


    /**
     * @param $id
     * @return IAuthIdentityWithPassword
     */
    public function findIdentity($id);
}
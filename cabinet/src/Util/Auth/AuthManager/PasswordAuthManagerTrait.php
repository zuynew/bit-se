<?php


namespace Util\Auth\AuthManager;


use Util\Auth\AuthIdentity\IAuthIdentityWithPassword;
use Util\Auth\AuthIdentityRepository\IAuthIdentityWithPasswordRepository;
use Util\Auth\Exception\AuthIdentityNotFound;
use Util\Auth\Exception\IncorrectPasswordException;
use Util\Security\ISecurityManager;

trait PasswordAuthManagerTrait
{
    /**
     * @param $id
     * @param string $password
     * @return IAuthIdentityWithPassword
     * @throws IncorrectPasswordException
     * @throws AuthIdentityNotFound
     */
    public function findAndValidateByIdentityAndPassword($id, string $password)
    {
        try
        {
            $authIdentity = $this->_getAuthIdentityRepository()->findIdentity($id);
        }
        catch (\Throwable $e)
        {
            throw new AuthIdentityNotFound("", 0, $e);
        }

        if (!$this->_getSecurityManager()->validatePassword($password, $authIdentity->getPassword()))
        {
            throw new IncorrectPasswordException();
        }

        return $authIdentity;
    }

    abstract protected function _getSecurityManager(): ISecurityManager;

    /**
     * @return IAuthIdentityWithPasswordRepository
     */
    abstract protected function _getAuthIdentityRepository();
}
<?php


namespace Util\Auth\AuthManager;


use Util\Auth\AuthIdentity\IAuthIdentityWithPassword;

interface IPasswordAuthManager
    extends IAuthManager
{
    /**
     * @param $id
     * @param string $password
     * @return IAuthIdentityWithPassword
     */
    public function loginByIdentityAndPassword($id, string $password);

}
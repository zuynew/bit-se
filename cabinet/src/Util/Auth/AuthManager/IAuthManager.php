<?php


namespace Util\Auth\AuthManager;


use Util\Auth\AuthIdentity\IAuthIdentity;

interface IAuthManager
{

    public function logout(): void;

    public function isGuest(): bool;

    /**
     * @return IAuthIdentity
     */
    public function getAuthIdentity();
}
<?php


namespace Util\Auth\AuthManager;


use Entity\User;
use Repository\UserRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Util\Auth\Exception\UnauthenticatedUserException;
use Util\Security\ISecurityManager;

class WebAuthManager
    implements IUserPasswordAuthManager
{
    const SESSION_KEY_AUTH_IDENTITY = 'auth_identity';

    use PasswordAuthManagerTrait;

    /** @var SessionInterface  */
    protected $_session;
    /**
     * @var UserRepository
     */
    protected $_authIdentityRepository;
    /** @var ISecurityManager */
    protected $_securityManager;

    public function __construct(
        SessionInterface $session,
        UserRepository $authIdentityRepository,
        ISecurityManager $securityManager
    )
    {
        $this->_session = $session;
        $this->_authIdentityRepository = $authIdentityRepository;
        $this->_securityManager = $securityManager;
    }

    /**
     * @param $email
     * @param string $password
     * @return User
     * @throws \Util\Auth\Exception\AuthIdentityNotFound
     * @throws \Util\Auth\Exception\IncorrectPasswordException
     */
    public function loginByIdentityAndPassword($email, string $password): User
    {
        /** @var User $user */
        $user = $this->findAndValidateByIdentityAndPassword($email, $password);

        $this->_session->set(static::SESSION_KEY_AUTH_IDENTITY, $user->getId());

        return $user;
    }


    /**
     * @throws UnauthenticatedUserException
     */
    public function logout(): void
    {
        if ($this->isGuest())
        {
            throw new UnauthenticatedUserException();
        }

        $this->_session->remove(static::SESSION_KEY_AUTH_IDENTITY);
    }

    public function isGuest(): bool
    {
        return
            !$this->_session->has(static::SESSION_KEY_AUTH_IDENTITY);
    }

    /**
     * @return User
     * @throws UnauthenticatedUserException
     * @throws \Exception\EntityNotFound
     */
    public function getAuthIdentity(): User
    {
        if ($this->isGuest())
        {
            throw new UnauthenticatedUserException();
        }

        return $this->_getAuthIdentityRepository()
            ->findIdentity($this->_session->get(static::SESSION_KEY_AUTH_IDENTITY));
    }

    protected function _getSecurityManager(): ISecurityManager
    {
        return $this->_securityManager;
    }

    /**
     * @return UserRepository
     */
    protected function _getAuthIdentityRepository()
    {
        return $this->_authIdentityRepository;
    }
}
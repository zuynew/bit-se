<?php


namespace Util\Auth\AuthManager;


use Entity\User;

interface IUserPasswordAuthManager extends IPasswordAuthManager
{

    /**
     * @param string $email
     * @param string $password
     * @return User
     */
    public function loginByIdentityAndPassword($email, string $password): User;


    /**
     * @return User
     */
    public function getAuthIdentity(): User;
}
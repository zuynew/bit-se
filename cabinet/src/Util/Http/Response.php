<?php


namespace Util\Http;


use Psr\Http\Message\StreamInterface;
use Slim\Http\Cookies;
use Slim\Interfaces\Http\HeadersInterface;

class Response extends \Slim\Http\Response
{


    protected $_cookies;

    public function __construct($status = 200, HeadersInterface $headers = null, StreamInterface $body = null, Cookies $cookies = null)
    {
        parent::__construct($status, $headers, $body);

        $this->_cookies = $cookies ?? new Cookies();
        if ($cookies)
        {
            $this->_setCookie($this->_cookies);
        }
    }


    public function withCookie(string $name, $value, \DateTimeInterface $expires = null)
    {
        $clone = clone $this;
        $value = ['value' => (string)$value];
        if ($expires)
        {
            $value['expires'] = $expires->format('r');
        }

        $clone->_cookies->set($name, $value);

        $clone->_setCookie($clone->_cookies);

        return $clone;
    }

    protected function _setCookie(Cookies $cookies)
    {
        $headers = $this->headers;
        $headers->set('Set-Cookie', $cookies->toHeaders());
        $this->headers = $headers;
    }
}
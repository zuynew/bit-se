<?php


namespace Util\Routing;


use Slim\App;
use Util\Semigroup\ISemigroup;

interface IRouterGroupExpr extends ISemigroup
{
    public function apply(App $app): App;
}
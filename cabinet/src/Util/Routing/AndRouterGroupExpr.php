<?php


namespace Util\Routing;


use Slim\App;
use Util\Semigroup\ISemigroup;

class AndRouterGroupExpr implements IRouterGroupExpr
{
    /**
     * @var IRouterGroupExpr
     */
    protected $_a;
    /**
     * @var IRouterGroupExpr
     */
    protected $_b;

    /**
     * AndRouterGroupBuilderExpr constructor.
     * @param IRouterGroupExpr $a
     * @param IRouterGroupExpr $b
     */
    public function __construct($a, $b)
    {
        $this->_a = $a;
        $this->_b = $b;
    }

    public function apply(App $app): App
    {
        return $this->_a->apply($this->_b->apply($app));
    }

    public function combine(ISemigroup $routerGroupBuilderExpr): ISemigroup
    {
        return new static($this, $routerGroupBuilderExpr);
    }
}
<?php


namespace Util\Routing;


use Slim\App;
use Util\Semigroup\ISemigroup;

class RouterGroup
    implements IRouterGroupExpr
{

    protected $_routes = [];
    protected $_pattern = '';

    /**
     * Create a new RouteGroup
     *
     * @param string   $pattern  The pattern prefix for the group
     */
    public function __construct(string $pattern = '')
    {
        $this->_pattern = $pattern;
    }


    public function apply(App $app): App
    {
        $app0 = clone $app;

        foreach ($this->_routes as $pattern => $routes)
        {
            foreach ($routes as $method => $callable)
            {
                $app0->map([$method], $this->_pattern.$pattern, $callable);
            }
        }

        return $app0;
    }

    protected function _addRoute(array $methods, string $pattern, callable $callable): RouterGroup
    {
        $self = clone $this;
        $self->_routes[$pattern] = array_replace_recursive(
            $self->_routes[$pattern] ?? [],
            array_fill_keys($methods, $callable)
        );

        return $self;
    }

    public function get(string $pattern, callable $callable): RouterGroup
    {
        return $this->_addRoute(['GET'], $pattern, $callable);
    }

    public function post(string $pattern, callable $callable): RouterGroup
    {
        return $this->_addRoute(['POST'], $pattern, $callable);
    }

    public function put(string $pattern, callable $callable): RouterGroup
    {
        return $this->_addRoute(['PUT'], $pattern, $callable);
    }

    public function patch(string $pattern, callable $callable): RouterGroup
    {
        return $this->_addRoute(['PATCH'], $pattern, $callable);
    }

    public function delete(string $pattern, callable $callable): RouterGroup
    {
        return $this->_addRoute(['DELETE'], $pattern, $callable);
    }

    public function options(string $pattern, callable $callable): RouterGroup
    {
        return $this->_addRoute(['OPTIONS'], $pattern, $callable);
    }

    public function any( string $pattern, callable $callable): RouterGroup
    {
        return $this->_addRoute(['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS'], $pattern, $callable);
    }

    public function combine(ISemigroup $routerGroupBuilderExpr): ISemigroup
    {
        return new AndRouterGroupExpr($this, $routerGroupBuilderExpr);
    }
}
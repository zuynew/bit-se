<?php

namespace Util\DependencyInjection;


use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Entity\User;
use Service\LoginService;
use Slim\Http\Headers;
use Slim\Http\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Util\Auth\AuthManager\WebAuthManager;
use Util\Config\AppConfiguration;
use Util\Config\ConfigFactory;
use Util\Config\TypedConfiguration;
use Util\Http\Response;
use Util\Security\DumbSecurityManager;
use Util\View\Twig;

/**
 * Class ContainerFacade
 *
 * @property-read \Util\Config\TypedConfiguration config
 * @property-read \Doctrine\ORM\EntityManager db
 * @property-read \Doctrine\DBAL\Connection dwhDb
 * @property-read Twig twig
 * @property-read Request $request
 * @property-read array settings
 * @property-read LoginService loginService
 * @property-read Session session
 * @property-read WebAuthManager webAuthManager
 * @property-read DumbSecurityManager securityManager
 * @property-read \IModelAPIGateway apiGateway
 */
class ContainerFacade extends \Slim\Container
{

    public function __construct(array $values = [])
    {
        parent::__construct($values);


        $this['response'] = function (ContainerFacade $container) {
            $headers = new Headers(['Content-Type' => 'text/html; charset=UTF-8']);
            $response = new Response(200, $headers);

            return $response->withProtocolVersion($container->settings['httpVersion']);
        };

        $this['config'] = function (): TypedConfiguration {
            return ConfigFactory::load(null, AppConfiguration::getConfigTreeBuilder());
        };


        $this['db'] = function (ContainerFacade $container): EntityManager {

            $paths = [
                SRC_PATH . '/Entity'
            ];

            $config = $container->config;

            $isDevMode = $config->getBoolean("is_dev");

            $dbParams = array_replace_recursive(
                $config->getArray("db"),
                [
                    'driver'   => 'pdo_pgsql',
                ]
            );

            $metaConfig = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
            return EntityManager::create($dbParams, $metaConfig);
        };


        $this['twig'] = function (ContainerFacade $container) {
            return new Twig(SRC_PATH . '/templates', [
                'cache' => $container->config
                    ->getBoolean("is_dev") ? false : $container->config->getString('twig.cache_path')
            ]);
        };


        $this['session'] = function (ContainerFacade $container)
        {
            $session = new Session();

            if (!$session->isStarted())
            {
                $session->start();
            };

            return $session;
        };

        $this['webAuthManager'] = function (ContainerFacade $container)
        {
            return new WebAuthManager(
                $this->session,
                $this->db->getRepository(User::class),
                $this->securityManager
            );
        };

        $this['securityManager'] = function (ContainerFacade $container)
        {
            return new DumbSecurityManager();
        };

        $this['loginService'] = function (ContainerFacade $container)
        {
            return new LoginService($this->webAuthManager);
        };

        $this['apiGateway'] = function (ContainerFacade $container)
        {
            return new \DumbModelAPIGateway(
                $this->config->getString("prediction_model_service.host"),
                $this->config->getInt("prediction_model_service.port")
            );
        };
    }



}
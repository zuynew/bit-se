<?php

namespace Util\Security;

interface ISecurityManager
{
    public function generatePasswordHash(string $password): string ;

    public function validatePassword(string $password, string $hash): bool;
}
<?php


namespace Util\Security;

//TODO Перепистать на что то настоящее
class DumbSecurityManager
    implements ISecurityManager
{

    public function generatePasswordHash(string $password): string
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public function validatePassword(string $password, string $hash): bool
    {
        return hash_equals($hash, crypt($password, $hash));
    }
}
<?php


namespace Controller;


use Util\Routing\IRouterGroupExpr;

abstract class AbstractController
{

    abstract public function getRouteGroup(): IRouterGroupExpr;

}
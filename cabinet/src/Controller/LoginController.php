<?php


namespace Controller;


use Doctrine\ORM\EntityManager;
use Entity\User;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Request;
use Util\Auth\AuthManager\WebAuthManager;
use Util\Http\Response;
use Util\Routing\IRouterGroupExpr;
use Util\Routing\RouterGroup;
use Util\Security\ISecurityManager;
use Util\View\Twig;

class LoginController extends AbstractController
{

    /**
     * @var Twig
     */
    protected $_twig;
    /**
     * @var WebAuthManager
     */
    protected $_webAuthManager;
    /**
     * @var ISecurityManager
     */
    private $securityManager;
    /**
     * @var EntityManager
     */
    private $em;

    public function getRouteGroup(): IRouterGroupExpr
    {
        return (new RouterGroup(''))
            ->get('/login', [$this, 'actionLogin'])
            ->post('/login', [$this, 'actionSubmitLogin'])
            ->get('/logout', [$this, 'actionLogout'])
            ->post('/createUser', [$this, 'actionCreateUser']);
    }

    /**
     * LoginController constructor.
     * @param WebAuthManager $webAuthManager
     * @param Twig $twig
     * @param ISecurityManager $securityManager
     */
    public function __construct(
        WebAuthManager $webAuthManager,
        Twig $twig,
        ISecurityManager $securityManager,
        EntityManager $em)
    {
        $this->_twig = $twig;
        $this->_webAuthManager = $webAuthManager;
        $this->securityManager = $securityManager;
        $this->em = $em;
    }

    public function actionLogin(Request $request, Response $response, array $params): ResponseInterface
    {
        return $this->_webAuthManager->isGuest()
            ? $this->_twig->render($response, "login/login-form.twig", [])
            : $response->withRedirect('/index', 302);
    }


    public function actionSubmitLogin(Request $request, Response $response, array $params): ResponseInterface
    {
        $email = $request->getParam("email");
        $password = $request->getParam("password");

        if (!isset($email) || !isset($password)) {
            throw new \Exception("Email and password required");
        }

        $user = $this->_webAuthManager->loginByIdentityAndPassword($email, $password);

        return $response->withRedirect("/index", 302);
    }


    public function actionLogout(Request $request, Response $response, array $params): ResponseInterface
    {
        $this->_webAuthManager->logout();

        return $response->withRedirect("/login", 302);
    }

    public function actionCreateUser(Request $request, Response $response, array $params): ResponseInterface
    {

        $email = $request->getParam("email");
        $password = $request->getParam("password");

        if (!isset($email) || !isset($password)) {
            throw new \Exception("Email and password required");
        }

        $user = new User();

        $user->email = $email;

        $user->passwordHash = $this->securityManager->generatePasswordHash($password);

        $this->em->persist($user);

        $this->em->flush();

        return $response->withJson(['user' => $user, 'password' => $password]);
    }
}
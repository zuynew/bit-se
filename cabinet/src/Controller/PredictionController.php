<?php
/**
 * Created by PhpStorm.
 * User: Mark
 * Date: 24.10.2017
 * Time: 16:37
 */

namespace Controller;


use DumbModelAPIGateway;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Request;
use Util\Http\Response;
use Util\Routing\IRouterGroupExpr;
use Util\Routing\RouterGroup;
use Util\View\Twig;

class PredictionController extends AbstractController
{
    private $_twig;

    protected $_dumbModelAPIGateway;

    public function getRouteGroup(): IRouterGroupExpr
    {
        return (new RouterGroup(''))
            ->get('/predict-by-user-id', [$this, 'actionPredictByUserId'])
            ->get('/predict-by-item-id', [$this, 'actionPredictByItemId'])
            ->get('/repeat-purchase-prediction', [$this, 'actionPredictRepeatPurchase'])
            ->post('/predict-by-user-id', [$this, 'submitPredictByUserId'])
            ->post('/predict-by-item-id', [$this, 'submitPredictByItemId'])
            ->post('/repeat-purchase-prediction', [$this, 'submitPredictRepeatPurchase'])
            ->get('/index', [$this, 'actionShowMainPredictionPage'])
            ->post('/index', [$this, 'actionShowMainPredictionPage']);
    }

    /**
     * PredictionController constructor.
     * @param Twig $_twig
     * @param DumbModelAPIGateway $dumbModelAPIGateway
     */
    public function __construct(Twig $_twig, \IModelAPIGateway $dumbModelAPIGateway)
    {
        $this->_twig = $_twig;
        $this->_dumbModelAPIGateway = $dumbModelAPIGateway;
    }

    public function actionPredictByUserId(Request $request, Response $response, array $params): ResponseInterface
    {
        return $this->_twig->render($response, "prediction/predict-by-user-id.twig", []);
    }


    public function actionPredictByItemId(Request $request, Response $response, array $params): ResponseInterface
    {
        return $this->_twig->render($response, "prediction/predict-by-item-id.twig", []);
    }

    public function actionPredictRepeatPurchase(Request $request, Response $response, array $params): ResponseInterface
    {
        return $this->_twig->render($response,
            "prediction/repeat-purchase-prediction.twig",
            []);

    }

    public function submitPredictByUserId(Request $request, Response $response, array $params): ResponseInterface
    {
        $inputUserId = $request->getParam("userId");

        $itemList = $this->_dumbModelAPIGateway->predictByUserId($inputUserId);

        return $this->_twig->render($response, "result/item-result-prediction.twig", ["itemIds" => $itemList]);
    }


    public function submitPredictByItemId(Request $request, Response $response, array $params): ResponseInterface
    {
        $inputItemId = $request->getParam("itemId");

        $userList = $this->_dumbModelAPIGateway->predictByItemId($inputItemId);

        return $this->_twig->render($response, "result/user-result-prediction.twig", ["userIds" => $userList]);
    }

    public function submitPredictRepeatPurchase(Request $request, Response $response, array $params): ResponseInterface
    {
        $inputUserListSize = $request->getParam("userListSize");

        $userList = $this->_dumbModelAPIGateway->predictRepeatPurchase(10,$inputUserListSize);

        return $this->_twig->render($response,
            "result/repeat-purchase-result-prediction",
            ["userIds" => $userList]);

    }

    public function actionShowMainPredictionPage(Request $request, Response $response, array $params): ResponseInterface
    {
        return $this->_twig->render($response, "index.twig", []);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Mark
 * Date: 01.11.2017
 * Time: 22:02
 */

namespace Controller;



use Psr\Http\Message\ResponseInterface;
use Slim\Http\Request;
use Util\Http\Response;
use Util\Routing\IRouterGroupExpr;
use Util\Routing\RouterGroup;

class PromotionController extends AbstractController
{


    public function getRouteGroup(): IRouterGroupExpr
    {
        return (new RouterGroup(''))->post('/send-message',[$this, 'submitSendMessageForm']);
    }

    /**
     * PromotionController constructor.
     */
    public function __construct()
    {
    }

    public function submitSendMessageForm(Request $request, Response $response, array $params): ResponseInterface
    {
        $inputMessage = $request->getParam("promoMessage");

        return $response->withJson("$inputMessage");
    }


}
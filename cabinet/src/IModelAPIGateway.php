<?php
/**
 * Created by PhpStorm.
 * User: Mark
 * Date: 26.10.2017
 * Time: 15:48
 */
interface IModelAPIGateway
{
    public function predictByItemId(int $inputItemId): array;

    public function predictByUserId(int $inputUserId) : array;

    public function predictRepeatPurchase(int $merchantId, int $inputSizeOfUserList): array;

}
<?php


namespace Repository;


use Doctrine\ORM\EntityRepository;
use Entity\User;
use Exception\EntityNotFound;
use Util\Auth\AuthIdentityRepository\IAuthIdentityWithPasswordRepository;

/**
 * Class UserRepository
 * @package Repository
 *
 */
class UserRepository
    extends EntityRepository
    implements IAuthIdentityWithPasswordRepository
{
    /**
     * @param string $email
     * @return User
     * @throws EntityNotFound
     */
    public function findIdentity($email): User
    {
        $user = $this->findOneBy(['email' => $email]);

        if (!($user instanceof User))
        {
            throw new EntityNotFound();
        }

        return $user;
    }
}
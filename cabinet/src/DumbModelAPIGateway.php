<?php
/**
 * Created by PhpStorm.
 * User: Mark
 * Date: 26.10.2017
 * Time: 17:05
 */

class DumbModelAPIGateway implements IModelAPIGateway
{
    /** @var \GuzzleHttp\Client  */
    protected $_client;


    /**
     * DumbModelAPIGateway constructor.
     * @param string $host
     * @param int $port
     */
    public function __construct(string $host, int $port)
    {
        $this->_client = new \GuzzleHttp\Client(
            ['base_uri' => $host.':'.$port]
        );
    }

    public function predictByItemId(int $inputItemId): array
    {
        $response = $this->_client->get("/predictUsersByItemId/$inputItemId/10");
        return json_decode($response->getBody()->getContents(), false);
    }

    public function predictByUserId(int $inputUserId): array
    {
        $response = $this->_client->get("/predictItemsByUserId/$inputUserId/10");
        return json_decode($response->getBody()->getContents(), false);
    }

    public function predictRepeatPurchase(int $inputMerchantId,int $inputSizeOfUserList): array
    {
        $response = $this->_client->get("/predictRepeatableBuy/$inputMerchantId/$inputSizeOfUserList");
        return json_decode($response->getBody()->getContents(), false);
    }
}
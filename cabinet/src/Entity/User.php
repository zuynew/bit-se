<?php

namespace Entity;

use Util\Auth\AuthIdentity\IAuthIdentityWithPassword;


/**
 * Class User
 * @package User
 *
 *
 * @Entity(repositoryClass="Repository\UserRepository")
 * @Table(name="tbl_user")
 *
 */
class User
    implements IAuthIdentityWithPassword
{

    /**
     * @var int
     * @Id
     * @GeneratedValue (strategy = "IDENTITY")
     * @Column(name="`id`", type="integer")
     */
    public $id;

    /**
     * @var string
     * @Column(name="`email`", type="string")
     */
    public $email;

    /**
     * @var string
     * @Column(name="`password_hash`",type="string")
     */
    public $passwordHash;

    public function getId(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->passwordHash;
    }
}
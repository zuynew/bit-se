<?php

use Util\DependencyInjection\ContainerFacade as Container;
use Slim\App as Server;

class App
{
    protected $_server;

    public function __construct()
    {
        $container = new Container();

        /** @var \Util\Routing\RouterGroup $routes */
        $routes = (new \Util\NonEmptyList\NonEmptyList(
            (new \Controller\LoginController(
                $container->webAuthManager,
                $container->twig,
                $container->securityManager,
                $container->db
            ))->getRouteGroup(),
            (new \Controller\PredictionController(
                $container->twig,
                $container->apiGateway
            )
            )->getRouteGroup(),
            (new \Controller\PromotionController(
            )
            )->getRouteGroup()
        ))
        ->fold();

        $this->_server = $routes->apply(new Server($container));
    }

    public function run()
    {
        $this->_server->run();
    }
}